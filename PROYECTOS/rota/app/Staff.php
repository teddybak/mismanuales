<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table = "staff";
    protected $fillable = [];

    public static function getStaff()
    {
        $return_json["staff"] = Staff::all();
        $return_json["total_staff"] = count(Staff::all());
        $return_json["operation_status"] = 200;
        return $return_json;
    }

}
