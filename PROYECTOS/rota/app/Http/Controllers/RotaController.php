<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rota;
use App\Shift;
use DB;

class RotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rotas = Rota::getRotas();
        return view('rota', $rotas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * get dataills about an specific rota id
     *
     * @param  int  $rota_id
     */
    public function rotainfo($rota_id)
    {
        $shifts = DB::select("select shifts.id as shift_id, rota_id, staff_id, staff.first_name,shifts.start_time as st, 
        shifts.end_time as et, shift_breaks.start_time as break_start, shift_breaks.end_time, DAYNAME(shifts.end_time) as dayname,
        DATE_FORMAT(shifts.end_time,'%d/%m/%Y') as fecha from shifts

        left join  shift_breaks on shifts.id = shift_breaks.shift_id

        left join staff on shifts.staff_id = staff.id

        where shifts.rota_id = $rota_id order by shifts.start_time");

        $shifts = json_decode(json_encode($shifts), true);

        $array_final = array(); // array with all necesaries values;


        foreach ($shifts as $key_level_1 => $value_level_1) {

            $array_final[$value_level_1["dayname"]][$value_level_1["shift_id"]]["shift_id"] = $value_level_1['shift_id'];
            $array_final[$value_level_1["dayname"]][$value_level_1["shift_id"]]["staff_id"] = $value_level_1['staff_id'];
            $array_final[$value_level_1["dayname"]][$value_level_1["shift_id"]]["day_week"] = $value_level_1['dayname'];
            $array_final[$value_level_1["dayname"]][$value_level_1["shift_id"]]["staff_name"] = $value_level_1['first_name'];
            $array_final[$value_level_1["dayname"]][$value_level_1["shift_id"]]["start_time"] = $value_level_1["st"];
            $array_final[$value_level_1["dayname"]][$value_level_1["shift_id"]]["end_time"] = $value_level_1["et"];
             

            // break time
            if (!empty($value_level_1["break_start"])) {
                $array_final[$value_level_1["dayname"]][$value_level_1["shift_id"]]["break_start"] = $value_level_1["break_start"];
                $array_final[$value_level_1["dayname"]][$value_level_1["shift_id"]]["break_end"] = $value_level_1["end_time"];
            }

        }

        print "<h1>More than one employee in the same day</h1>";

        foreach ($array_final as $key_level_1 => $value_level_1) {

            if (count($value_level_1) > 1) {
                $opeador1 = '';
                $opeador2 = '';

                foreach ($value_level_1 as $key_level_2 => $value_level_2) {

                    foreach ($value_level_2 as $key_level_3 => $value_level_3) {

                        print "\t";
                        print "<b>" . $key_level_3 . "</b>";
                        print "\t";
                        print $value_level_3;
                        print "\t";
                    }

                    print "<br>";
                }
            }

            print "<br>";
        }


        print "<h1>One Employee at shop</h1>";
        foreach ($array_final as $key_level_1 => $value_level_1) {

            if (count($value_level_1) == 1) {

                foreach ($value_level_1 as $key_level_2 => $value_level_2) {
                    foreach ($value_level_2 as $key_level_3 => $value_level_3) {
                        print "\t";
                        print "<b>" . $key_level_3 . "</b>";
                        print "\t";
                        print $value_level_3;
                        print "\t";
                    }
                    print "<br>";
                }
            }
            print "<br>";
        }

        $array_final_json = json_encode($array_final);
        //echo var_dump($array_final_json);

        $process_array = array();
        $days = array_keys($array_final);
        $counter = 1;

        //Obtengo todos los dias
        $days = array_keys($array_final);
        //Relleno la tabla process_array con los dias comenzando desde el dia 1 que es Lunes
        foreach ($days as $day) {
            $process_array[$counter] = $day;
            $counter++;
        }


        //var_dump($process_array);
        //Quiero obtener los keys dentro de los dias
        $process_array2 = $process_array;

        print "<h1>Testing</h1>";

        foreach ($process_array2 as $key) {

            if ($key == 'Monday') {
                $array_shift_day = array_keys($array_final["Monday"]);

                print "<h3>Monday</h3>";
                 //single staff all day
                if (count($array_shift_day) == 1) {

                    $start_time_first_staff = $array_final["Monday"][$array_shift_day[0]]['start_time'];
                    $finish_time_first_staff = $array_final["Monday"][$array_shift_day[0]]['end_time'];

                    $hours = $startTime = strtotime($finish_time_first_staff) - strtotime($start_time_first_staff);

                    echo $time = date("g:i", $hours) . " hours shift";
                    print "<br>";

                    print " YOU ARE IN SCENARIO 1 ";
                    print " <br> ";
                    print " TOTAL EMPLOYEE " . count($array_shift_day);

                }
                print "<br>";
            }

            if ($key == 'Tuesday') {
                $array_shift_day = array_keys($array_final["Tuesday"]);
                $start_time_first_staff = $array_final["Tuesday"][$array_shift_day[0]]['start_time'];
                $finish_time_first_staff = $array_final["Tuesday"][$array_shift_day[0]]['end_time'];

                $start_time_second_staff = $array_final["Tuesday"][$array_shift_day[1]]['start_time'];
                $finish_time_second_staff = $array_final["Tuesday"][$array_shift_day[1]]['end_time'];

                $hours1 = $startTime = strtotime($finish_time_first_staff) - strtotime($start_time_first_staff);
                $hours2 = $startTime = strtotime($finish_time_second_staff) - strtotime($start_time_second_staff);

                print "<h3>Tuesday</h3>";
                echo $time = date("g:i", $hours1) . " hours first shift ";
                echo $time = date("g:i", $hours2) . " hours second shift";
                print "<br>";
                print "Numbers employees " . count($array_shift_day);
                print "<br>";

                // break time
                if (!empty($array_final["Tuesday"][0]["break_start"])) {
                    print "no esta vacio";
                }



                $this->checkDates($start_time_first_staff, $finish_time_first_staff, $start_time_second_staff, $finish_time_second_staff);
                print "<br>";
            }

            if ($key == 'Wednesday') {
                $array_shift_day = array_keys($array_final["Wednesday"]);
                $start_time_first_staff = $array_final["Wednesday"][$array_shift_day[0]]['start_time'];
                $finish_time_first_staff = $array_final["Wednesday"][$array_shift_day[0]]['end_time'];

                $start_time_second_staff = $array_final["Wednesday"][$array_shift_day[1]]['start_time'];
                $finish_time_second_staff = $array_final["Wednesday"][$array_shift_day[1]]['end_time'];

                $hours1 = $startTime = strtotime($finish_time_first_staff) - strtotime($start_time_first_staff);
                $hours2 = $startTime = strtotime($finish_time_second_staff) - strtotime($start_time_second_staff);
                print "<h3>Wednesday</h3>";
                echo $time = date("g:i", $hours1) . " hours first shift ";
                echo $time = date("g:i", $hours2) . " hours second shift";
                print "<br>";

                print "Numbers employees " . count($array_shift_day);
                print "<br>";

                $this->checkDates($start_time_first_staff, $finish_time_first_staff, $start_time_second_staff, $finish_time_second_staff);
                print "<br>";
            }

            if ($key == 'Thursday') {


                $array_shift_day = array_keys($array_final["Thursday"]);
                $start_time_first_staff = $array_final["Thursday"][$array_shift_day[0]]['start_time'];
                $finish_time_first_staff = $array_final["Thursday"][$array_shift_day[0]]['end_time'];
                print "<h3>Thursday</h3>";
                print "Numbers employees " . count($array_shift_day);
                print "<br>";




                $hours1 = $startTime = strtotime($finish_time_first_staff) - strtotime($start_time_first_staff);
                $hours2 = $startTime = strtotime($finish_time_second_staff) - strtotime($start_time_second_staff);

                echo $time = date("g:i", $hours1) . " hours first shift ";
                echo $time = date("g:i", $hours2) . " hours second shift";
                print "<br>";
                echo date('g', strtotime($array_final["Thursday"][$array_shift_day[1]]["break_end"]) - strtotime($array_final["Thursday"][$array_shift_day[1]]["break_start"])) . " Extra at shop first shift";
                echo "<br>";
                echo date('g', strtotime($array_final["Thursday"][$array_shift_day[0]]["break_end"]) - strtotime($array_final["Thursday"][$array_shift_day[0]]["break_start"])) . " Extra at shop second shift";

                //break time
                print "<h3>Breaks for each staff</h3>";
                print $array_final["Thursday"][$array_shift_day[0]]["break_start"];
                print "<br>";
                print $array_final["Thursday"][$array_shift_day[0]]["break_end"];
                print "<br>";
                print $array_final["Thursday"][$array_shift_day[1]]["break_start"];
                print "<br>";
                print $array_final["Thursday"][$array_shift_day[1]]["break_end"];
                //end break time
                print "<br>";
                print "<br>";
                $start_time_second_staff = $array_final["Thursday"][$array_shift_day[1]]['start_time'];
                $finish_time_second_staff = $array_final["Thursday"][$array_shift_day[1]]['end_time'];

                $this->checkDates($start_time_first_staff, $finish_time_first_staff, $start_time_second_staff, $finish_time_second_staff);
                print "<br>";
            }
        }

    }


    /**
     * check scenario 
     *
     * @param  datetime startTime (first shift starting time)
     * @param  datetime endTime (first shift end time)
     * @param  datetime chkStartTime (second shift start time)
     * @param  datetime chkEndTime (second shift end time)
     */
    private function checkDates($startTime, $endTime, $chkStartTime, $chkEndTime)
    {
        $startTime = strtotime($startTime);
        $endTime = strtotime($endTime);

        $chkStartTime = strtotime($chkStartTime);
        $chkEndTime = strtotime($chkEndTime);

        if ($chkStartTime >= $startTime && $chkEndTime > $endTime && ($endTime - $chkStartTime == 0)) {
            print "YOU ARE IN THE SECOND SCENARIO";
            // more implementation ....
        }

        if ($chkStartTime < $endTime && $chkEndTime > $endTime) {
            print "YOU ARE IN THE TIRD SCENARIO";
            // more implementation ....
        }

        if ($startTime == $chkStartTime && $endTime == $chkEndTime) {
            print "YOU ARE IN THE FOUR SCENARIO";
            // more implementation ....
        }

    }




}
