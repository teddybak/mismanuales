<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ShiftBreak extends Model
{
    protected $table = "shift_breaks";


    public static function getShiftBreaks()
    {
        $return_json["shift_breaks"] = DB::table('shift_breaks')
        ->join('shifts', 'shift_breaks.shift_id', '=', 'shifts.id')
        ->join('staff', 'staff.id', '=', 'shifts.staff_id') 

        ->join('shops', 'staff.shop_id', '=', 'shops.id') 

        ->select('shifts.rota_id','shift_breaks.shift_id','shift_breaks.start_time','shift_breaks.end_time','staff.first_name','shops.name')
        ->get()
        ->toArray();
 
        return $return_json;
    } 
}
