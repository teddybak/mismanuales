<?php

namespace App;
use DB; 
use Carbon\Carbon;
 
use Illuminate\Database\Eloquent\Model;

class Shift extends Model
{
    protected $table = "shifts";


    public static function getShift()
    {
         $return_json["shift"] = DB::table('shifts')
        ->join('staff', 'shifts.staff_id', '=', 'staff.id')
        ->select('shifts.rota_id','shifts.start_time', 'shifts.end_time','staff.first_name')
        ->get();
 
 
        return $return_json;
    } 

}
