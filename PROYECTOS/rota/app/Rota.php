<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rota extends Model
{
    protected $table = "rotas";

    public static function getRotas()
    {
        $return_json["rotas"] = Rota::all();
        $return_json["total_rotas"] = count(Rota::all());
        $return_json["operation_status"] = 200;
        return $return_json;
    }


    public static function testing()
    {
        $to_time = strtotime("2008-12-13 10:42:00");
        $from_time = strtotime("2008-12-13 10:21:00");
        echo round(abs($to_time - $from_time) / 60, 2) . " minute";
    }
}
