<html>
    <head>
        <title>App Name - @yield('title')</title>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

        <!-- Static navbar -->
        <nav class="navbar navbar-inverse">
              <div class="container-fluid">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="/">Funhouse</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                  <ul class="nav navbar-nav">
                    <li class="active"><a href="/">Home</a></li>
      
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Rotas <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="/rota">List Rotas</a></li>
                        <li><a href="#">New Rota</a></li>
                        <li><a href="/rotainfo">Rota Info</a></li>
                      </ul>
                    </li>

                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Shifts <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="/shift">List Shifts</a></li>
                        <li><a href="#">Add Shift</a></li>
        
                      </ul>
                    </li>

                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Shifts Breaks <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="/break">List Breaks</a></li>
                        <li><a href="#">Add Breaks</a></li>
      
      
                      </ul>
                    </li>

                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Staff <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="#">List Staff</a></li>
                        <li><a href="#">Add Staff</a></li>
      
      
                      </ul>
                    </li>

                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Shop <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="#">List Shop</a></li>
                        <li><a href="#">Add Shop</a></li>
      
                      </ul>
                    </li>                                                        
                  </ul>

                </div><!--/.nav-collapse -->
              </div><!--/.container-fluid -->
            </nav> 

    </head>
    <body>
        @section('sidebar')
            
        @show

        <div class="container">
            @yield('content')
        </div>
 
 </body>
</html>