@extends('layouts.app')

@section('title', 'Page Title')

@section('sidebar')
    @parent     
@endsection

@section('content') 
<h1>Shifts in the current rota</h1>
<table class="table"> 
  <thead>
      <tr>
        <th>Rota Id</th>
        <th>Start Time</th> 
        <th>End Time</th> 
        <th>First Name</th>
      </tr>
    </thead>
    <tbody>
    <tr>
    @foreach ($shift as $shif)
          @foreach ($shif as $sh)
           <td>{{$sh}}</td>
          @endforeach  
          </tr>  
    @endforeach 
  </tbody>
  </table>

@endsection