@extends('layouts.app')

@section('title', 'Page Title')

@section('sidebar')
    @parent     
@endsection

@section('content')
<h1>Rota List</h1>

<table class="table"> 
  <thead>
      <tr>
        <th>Rota Id</th>
        <th>Week Commence Date</th> 
      </tr>
    </thead>
    <tbody>
    <tr>
@foreach ($rotas as $rota)
       <td>{{$rota['id']}}</td>
       <td>{{$rota['week_commence_date']}}</td>
      </tr> 
  @endforeach
  </tbody>
  </table>

@endsection