<?php
use App\Staff;
use App\Rota;
use App\Shift;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});


Route::resource('rota', 'RotaController');
Route::resource('shift', 'ShiftController');
Route::resource('break', 'BreakController');
Route::resource('shop', 'ShopController');
Route::resource('staff', 'StaffController');

Route::get('rotainfo/{rota_id}', 'RotaController@rotainfo');

